// defines pins numbers
const int stepX = 2;
const int dirX  = 5;
const int stepY = 3;
const int dirY  = 6;
const int stepZ = 4;
const int dirZ  = 7;
const int enPin = 8;
const int servoPin = 13;

// defines step length
const int stepduration = 1000;


void setup() {
  
  // Sets the pins as Outputs
  pinMode(stepX,OUTPUT);
  pinMode(dirX,OUTPUT);
  pinMode(stepY,OUTPUT);
  pinMode(dirY,OUTPUT);
  pinMode(stepZ,OUTPUT);
  pinMode(dirZ,OUTPUT);
  pinMode(enPin,OUTPUT);
  pinMode(servoPin,OUTPUT);

  digitalWrite(enPin,LOW);
  digitalWrite(dirX,HIGH);
  digitalWrite(dirY,LOW);
  digitalWrite(dirZ,HIGH);
}

//-------------------------------------------------------------------------

void loop() {

  // 360 deg rotation equals 2746,71 Steps, ( 1 deg = 7,63 steps ) - delay before start after plugging in power cable
  delay(5000);
  
  //close clamp - PWM ist immer 20ms, 1ms high = 0 deg, 2ms high = 180 deg
  for(int x = 0; x < 50; x++) {
  digitalWrite(servoPin,HIGH);
  delayMicroseconds(1000);
  digitalWrite(servoPin,LOW);
  delayMicroseconds(19000);
  }

  //rotate arm 3 out 90 deg
  digitalWrite(dirZ,LOW);
  stepinZ(687,1.5);
  
  //move arms 1 and 2 to correct position
  digitalWrite(dirX,HIGH);
  stepinX(150);
  digitalWrite(dirY,LOW);
  stepinY(400);
  
  //set staple - move arm 3 further until set
  digitalWrite(dirZ,LOW);
  stepinZ(500,1.5);
  
  //open clamp
  for(int x = 0; x < 50; x++) {
  digitalWrite(servoPin,HIGH);
  delayMicroseconds(1600);
  digitalWrite(servoPin,LOW);
  delayMicroseconds(18400);
  }

  //move arm 3 back up
  digitalWrite(dirZ,HIGH);
  stepinZ(500,1);
  
  //return arms 1 and 2
  digitalWrite(dirX,LOW);
  stepinX(150);
  digitalWrite(dirY,HIGH);
  stepinY(400);
  
  //rotate arm 3 back 90 deg
  digitalWrite(dirZ,HIGH);
  stepinZ(687,1);
  
  //wait for next command
  delay(10000);
}

//-------------------------------------------------------------------------

int stepinX(int i){
  for(int x = 0; x < i; x++) {
   digitalWrite(stepX,HIGH);
   delayMicroseconds(stepduration);
   digitalWrite(stepX,LOW);
   delayMicroseconds(stepduration);
  }
}

int stepinY(int i){
  for(int x = 0; x < i; x++) {
   digitalWrite(stepY,HIGH);
   delayMicroseconds(stepduration);
   digitalWrite(stepY,LOW);
   delayMicroseconds(stepduration);
  }
}

int stepinZ(int i, int v){
  for(int x = 0; x < i; x++) {
   digitalWrite(stepZ,HIGH);
   delayMicroseconds(v*stepduration);
   digitalWrite(stepZ,LOW);
   delayMicroseconds(v*stepduration);
  }
}
